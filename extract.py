import urllib.request
import json

def extracDeezer(playlist_id):
    response = urllib.request.urlopen('https://api.deezer.com/playlist/'+playlist_id)
    data = response.readlines()
    data = data[0].decode("utf-8")
    var = json.loads(data)
    f=open(var['title']+".csv","w+")
    print("[+] Writing songs in file",var['title'],".csv")
    for song in var['tracks']['data']:
        print("[+] Writing : "+song['title']+" from "+song['artist']['name'])
        f.write(song['title']+","+song['artist']['name']+"\n")
    f.close()

print("Playlist id :")
playlist = input(">")
extracDeezer(playlist)

