Music.py
========

It is a script that allow you to listen music from youtube by querying a song or by using a local playlist file.

Requirements
------------

```bash
pip3 install pafy
pip3 install youtube-dl
```

You also need to install vlc with the same arch as python (64bits or 32bits)

Playlist
--------

You can make a playlist in this format :

- A csv file with a "," to separate each entity
- First entity : music title
- Second entity : artist

Usage
-----

```bash
./music.py play playlist <playlistname>
./music.py play titre <title>:<artist>
```

The playlist name is the name of your file without the ".csv"

Future update
-------------

- Save a specific url for a song
- Extract a playlist from streaming platforms
