#!/usr/bin/env python3
import pafy
import vlc
import urllib.request
import time
import sys

volume = 50
class PlayList(object):
    def __init__(self,name):
        self.name=name+".csv"
    def play(self):
        f = open(self.name, "r")
        liste = f.readlines()
        f.close()
        for music in liste:
            music_name = music.split(",")[0]
            artist_name = music.split(",")[1]
            if(len(music.split(","))>2):
                url = music.split(",")[2]
                titre = Titre(music_name,artist_name,url)
            else:
                titre = Titre(music_name,artist_name)
            titre.play()
class Titre(object):
    #do query with title and artist
    def getUrl(self):
        query = self.name.replace(" ","+")
        query = query+self.artist.replace(" ","+")
        query = query.replace("&","%26")
        data=urllib.request.urlopen('https://www.youtube.com/results?search_query='+query)
        html = data.readlines()
        for line in html:
            line = str(line)
            if("/watch?v=" in line):
                url = line[line.find('href="')+len('href="'):line.rfind('" class=" yt')]
                print("url : ",url)
                url = "https://www.youtube.com"+url
                html=""
                return url
    def __init__(self,name,artist,url=""):
        self.name=name
        self.artist=artist
        if(url==""):
            self.url=self.getUrl()
        else:
            self.url=url
        self.volume=volume

    def play(self):
        video = pafy.new(self.url)
        best = video.getbestaudio()
        playurl = best.url
        Instance = vlc.Instance()
        player = Instance.media_player_new()
        Media = Instance.media_new(playurl)
        Media.get_mrl()
        player.set_media(Media)
        player.audio_set_volume(self.volume)
        player.play()
        print("[*] Playing :",self.name," from",self.artist)
        while(1):
            try:
                time.sleep(2)
                if(player.get_state()==vlc.State.Ended):
                    player.stop()
                    print("[*] Ended")
                    break
            except KeyboardInterrupt:
                player.stop()
                print("[*] Stoped")
                break

def main():
    if(sys.argv[1]=="play"):
        if(sys.argv[2]=="playlist"):
            playlist = PlayList(sys.argv[3])
            playlist.play()
        elif(sys.argv[2]=="titre"):
            titre_name=' '.join(sys.argv[3:]).split(":")[0]
            artist_name=' '.join(sys.argv[3:]).split(":")[1]
            titre = Titre(titre_name,artist_name)
            titre.play()
if __name__=="__main__":
    if(len(sys.argv)<2):
        print("""Usage :
        ./music.py play titre <name>:<artist>
        ./music.py play playlist <playlist name>""")
    else:
        main()
